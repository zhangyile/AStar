#ifndef ASTARPATH_H_
#define ASTARPATH_H_
#include "cocos2d.h"
USING_NS_CC;
#include "EstGrid.h"
static const int COST = 10;
static const int SLOPE = 14;
static const std::string obstacleLayer = "obstacle";
static const std::string roadLayer = "road";

class AStarPath
{
public:
	static AStarPath* getInstance();
	Vector<EstGrid*>* findShortestPath(Vec2 start, Vec2 dest, TMXTiledMap* map);
	Vec2 posToMapBlock(Vec2& vec, TMXTiledMap* map);
	/*EstGrid* findTopRight(EstGrid& parent, EstGrid& dest);
	EstGrid* findBottomRight(EstGrid& parent, EstGrid& dest);
	EstGrid* findBottomLeft(EstGrid& parent, EstGrid& dest);
	EstGrid* findTopLeft(EstGrid& parent, EstGrid& dest);*/
	
protected:
	AStarPath();
	~AStarPath();
	CC_DISALLOW_COPY_AND_ASSIGN(AStarPath);
	EstGrid* findTop(EstGrid& parent, Vec2& dest);
	EstGrid* findBottom(EstGrid& parent, Vec2& dest);
	EstGrid* findLeft(EstGrid& parent, Vec2& dest);
	EstGrid* findRight(EstGrid& parent, Vec2& dest);
	
	void clear();

	EstGrid* findRound(EstGrid* root);
	//Vec2代表地图中的坐标
	EstGrid* newGrid(Vec2& mapPos, Vec2& dest, EstGrid* parent, int spend);

	int calcDestCost(Vec2& start, Vec2& end);
	

	void openListToCloseList(EstGrid* grid);
	void closeListToOpenList(EstGrid* grid);
	EstGrid* findMinInOpenList();
	EstGrid* TryNextGrid(EstGrid* parent, Vec2& dest, Vec2& next);
	bool isContains(Vec2& vec, Vector<EstGrid*>& list);
	EstGrid* findGridInList(Vec2& vec, Vector<EstGrid*>& list);
private:
	static AStarPath* g_instantce;
	//包含方格可能使要途径的方格
	Vector<EstGrid*> _openList;
	Vector<EstGrid*> _closeList;
	TMXTiledMap* _map;
	TMXLayer* _obstacle;
	TMXLayer* _road;
	Vec2 _startPos;
	Vec2 _endPos;
	std::function<EstGrid*(EstGrid& parent, Vec2& des)> findFuncArray[4];
	int _line;
	int _row;
};

#endif
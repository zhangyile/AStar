#ifndef ESTGRID_H_
#define ESTGRID_H_
#include "cocos2d.h"
USING_NS_CC;

#define GET_SET_PROPERTY(varType,varName)\
const varType& get##varName(void){return this->varName;}\
void set##varName(const varType value){this->varName = value;}


class EstGrid : public Ref
{
public:
	EstGrid(Vec2& vec);
	~EstGrid();
	static EstGrid* create(Vec2& vec);
	GET_SET_PROPERTY(int, _f);
	GET_SET_PROPERTY(int, _g);
	GET_SET_PROPERTY(int, _h);
	GET_SET_PROPERTY(int, _gid);
	
	EstGrid* getParent(){ return _parent; }
	void setParent(EstGrid* value){ this->_parent = value; }
	
	Vec2& getPosInMap(){ return this->_pos;}
	void setPosInMap(Vec2& value){ this->_pos = value; }

	bool operator==(const EstGrid& eg)const;
	bool operator > (const EstGrid& eg)const;
	bool operator< (const EstGrid& eg)const;
private:
	int _f;
	int _g;
	int _h;
	int _gid;
	EstGrid* _parent;
	Vec2 _pos;
	CC_DISALLOW_COPY_AND_ASSIGN(EstGrid);
};
#endif

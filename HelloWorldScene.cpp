#include "HelloWorldScene.h"
#include "AStarPath.h"
USING_NS_CC;
HelloWorld::HelloWorld()
{}
HelloWorld::~HelloWorld()
{}
Scene* HelloWorld::createScene()
{
    // 'scene' is an autorelease object
    auto scene = Scene::create();
    
    // 'layer' is an autorelease object
    auto layer = HelloWorld::create();

    // add layer as a child to scene
    scene->addChild(layer);

    // return the scene
    return scene;
}

// on "init" you need to initialize your instance
bool HelloWorld::init()
{
    //////////////////////////////
    // 1. super init first
    if ( !Layer::init() )
    {
        return false;
    }
    
    Size visibleSize = Director::getInstance()->getVisibleSize();
    Vec2 origin = Director::getInstance()->getVisibleOrigin();

	EventListenerTouchOneByOne* listener = EventListenerTouchOneByOne::create();
	listener->setSwallowTouches(true);
	listener->onTouchBegan = std::bind(&HelloWorld::onTouchBegan, this, std::placeholders::_1, std::placeholders::_2);
	listener->onTouchMoved = std::bind(&HelloWorld::onTouchMoved, this, std::placeholders::_1, std::placeholders::_2);
	listener->onTouchEnded = std::bind(&HelloWorld::onTouchEnded, this, std::placeholders::_1, std::placeholders::_2);
	listener->onTouchCancelled = std::bind(&HelloWorld::onTouchCancelled, this, std::placeholders::_1, std::placeholders::_2);
	Director::getInstance()->getEventDispatcher()->addEventListenerWithSceneGraphPriority(listener, this);

	_map = TMXTiledMap::create("tmx/map.tmx");
	this->addChild(_map);

	_label = LabelTTF::create();
	_label->setString("Click anywhere on the screen");
	_label->setFontSize(30);
	_label->setPosition(visibleSize.width / 2, visibleSize.height*0.8);
	this->addChild(_label);

	_player = CCSprite::create("tmx/red.png");
	//_player->setAnchorPoint(ccp(0,0));
	Vec2 pos =_map->getLayer("road")->getPositionAt(Vec2(2, 5));
	_player->setPosition(pos.x+_player->getContentSize().width/2 , pos.y+_player->getContentSize().height/2);
	this->addChild(_player);

	_path = nullptr;

	this->schedule(std::bind(&HelloWorld::update,this,std::placeholders::_1), 0.5f,"HelloWorld");

    return true;
}

bool HelloWorld::onTouchBegan(Touch *touch, Event *unused_event)
{
	log("onTouchBegan");
	Vec2 dest = AStarPath::getInstance()->posToMapBlock(touch->getLocation(), _map);
	_label->setString(StringUtils::format("touch in map positon x:%f y:%f", dest.x, dest.y));
	Vec2 start = AStarPath::getInstance()->posToMapBlock(Vec2(_player->getPosition()),_map);
	_path = AStarPath::getInstance()->findShortestPath(start, dest, _map);

	return true;
}
void HelloWorld::onTouchMoved(Touch *touch, Event *unused_event)
{
	log("onTouchMoved");
}
void HelloWorld::onTouchEnded(Touch *touch, Event *unused_event)
{
	log("onTouchEnded");
}
void HelloWorld::onTouchCancelled(Touch *touch, Event *unused_event)
{
	log("onTouchCancelled");
}

void HelloWorld::update(float delta)
{
	if (_path&& !_path->empty())
	{
		//获取最后一个元素
		EstGrid* nextGrid = _path->back();
		//将坐标转换成全局
		Vec2 pos = _map->getLayer("road")->getPositionAt(nextGrid->getPosInMap());
		_player->setPosition(pos.x + _player->getContentSize().width / 2, pos.y + _player->getContentSize().height / 2);
		//删除最后一个坐标
		_path->popBack();
	}
	else
	{
		if (_path)
		{
			_path->clear();
			delete _path;
			_path = nullptr;
		}
		
	}
}
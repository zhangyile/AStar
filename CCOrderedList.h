#ifndef __CCORDEREDLIST_H_
#define __CCORDEREDLIST_H_
#include "base/ccMacros.h"
#include "base/CCRef.h"
#include<list>
#include <algorithm>
NS_CC_BEGIN 
//一个有序的list，元素从小到大排列
template<class T>
class OrderList
{
public:
	// ------------------------------------------
	// Iterators
	// ------------------------------------------
	typedef typename std::list<T>::iterator iterator;
	typedef typename std::list<T>::const_iterator const_iterator;

	typedef typename std::list<T>::reverse_iterator reverse_iterator;
	typedef typename std::list<T>::const_reverse_iterator const_reverse_iterator;

	iterator begin() { return _data.begin(); }
	const_iterator begin() const { return _data.begin(); }

	iterator end() { return _data.end(); }
	const_iterator end() const { return _data.end(); }

	const_iterator cbegin() const { return _data.cbegin(); }
	const_iterator cend() const { return _data.cend(); }

	reverse_iterator rbegin() { return _data.rbegin(); }
	const_reverse_iterator rbegin() const { return _data.rbegin(); }

	reverse_iterator rend() { return _data.rend(); }
	const_reverse_iterator rend() const { return _data.rend(); }

	const_reverse_iterator crbegin() const { return _data.crbegin(); }
	const_reverse_iterator crend() const { return _data.crend(); }

	explicit OrderList<T>() : _data()
	{
		static_assert(std::is_convertible<T, Ref*>::value, "Invalid Type for cocos2d::Vector<T>!");
	}

	~OrderList<T>()
	{
		CCLOGINFO("In the destructor of Vector.");
		clear();
	}

	OrderList<T>& operator=(OrderList<T>& other);
	OrderList<T>& operator=(const OrderList<T>& other);

	ssize_t size() const
	{
		return _data.size();
	}
	bool empty() const
	{
		return _data.empty();
	}
	void clear();
	void addRefForAllObjects();

	void insertElementByOrder(T data);
	

	T getMinElementInList()
	{
		return *_data.begin();
	}

private:
	std::list<T> _data;
};
NS_CC_END
#endif
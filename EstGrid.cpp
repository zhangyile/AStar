#include "EstGrid.h"

EstGrid::EstGrid(Vec2& vec):_pos(vec)
{
	_f = 0;
	_g = 0;
	_h = 0;
	_gid = 0;
	_parent = nullptr;
}

EstGrid::~EstGrid()
{
	CC_SAFE_RELEASE_NULL(this->_parent);
}

EstGrid* EstGrid::create(Vec2& vec)
{
	EstGrid* pRef = new EstGrid(vec);
	if (pRef)
	{
		pRef->autorelease();
		return pRef;
	}
	else
	{
		delete pRef;
		pRef = NULL;
		return NULL; 
	}
}

bool EstGrid::operator == (const EstGrid& eg)const
{
	if (this == &eg)
		return true;
	if (this->_pos== eg._pos)
		return true;
	
	return false;
}

bool EstGrid::operator > (const EstGrid& eg)const
{
	if (this->_f > eg._f)
		return true;
	return false;
}
bool EstGrid::operator< (const EstGrid& eg)const
{
	if (this->_f < eg._f)
		return true;
	return false;
}
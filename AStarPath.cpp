#include"AStarPath.h"
#include<math.h>
AStarPath* AStarPath::g_instantce = nullptr;

AStarPath::AStarPath() 
{
	findFuncArray[0] = std::bind(&AStarPath::findTop, this, std::placeholders::_1, std::placeholders::_2);
	findFuncArray[1] = std::bind(&AStarPath::findRight, this, std::placeholders::_1, std::placeholders::_2);
	findFuncArray[2] = std::bind(&AStarPath::findBottom, this, std::placeholders::_1, std::placeholders::_2);
	findFuncArray[3] = std::bind(&AStarPath::findLeft, this, std::placeholders::_1, std::placeholders::_2);
}

AStarPath::~AStarPath()
{

}

AStarPath* AStarPath::getInstance()
{
	if (g_instantce == nullptr)
	{
		g_instantce = new AStarPath();
	}
	return g_instantce;
}

Vector<EstGrid*>* AStarPath::findShortestPath(Vec2 start, Vec2 dest, TMXTiledMap* map)
{
	if (start == dest || map->getLayer(obstacleLayer)->getTileAt(dest))
		return nullptr;
	clear();
	_map = map;
	_obstacle = map->getLayer(obstacleLayer);
	_road = map->getLayer(roadLayer);
	_line = map->getMapSize().width;
	_row = map->getMapSize().height;
	
	_startPos = start;
	_endPos = dest;

	//将起始格子放入开放列表
	_openList.pushBack(new EstGrid(_startPos));

	EstGrid* isFound = nullptr;
	EstGrid* currentGrid = nullptr;
	/***********循环开始****************/
	while (!isFound&&!_openList.empty())
	{
		//第一 从"开放列表"中找出最小F值的格子放入"封闭列表"中
		currentGrid = findMinInOpenList();
		openListToCloseList(currentGrid);
		//查找当前格子(currentGrid)周围的格子
		isFound = this->findRound(currentGrid);
	}

	Vector<EstGrid*>* shortest = new Vector < EstGrid* > ;
	while (isFound)
	{
		shortest->pushBack(isFound);
		isFound = isFound->getParent();
	}
	//clear();
	return shortest;
}

EstGrid* AStarPath::findRound(EstGrid* root)
{
	for (int i = 0; i < 4; i++)
	{
		//查找上下左右格子
		EstGrid* result = this->findFuncArray[i](*root, _endPos);
		if (result && (result->getPosInMap() == _endPos))
			return result;
	}
	return nullptr;
}

EstGrid* AStarPath::TryNextGrid(EstGrid* parent, Vec2& dest, Vec2& next)
{
	//如果不在封闭列表中
	//接着判断是否在开放列表中
	EstGrid* existGrid = findGridInList(next, _openList);
	if (existGrid)
	{//已经在开放列表中，判断从当前点到达该点，G值是否变小
		if (parent->get_g() + COST < existGrid->get_g())
		{
			existGrid->setParent(parent);
			existGrid->set_g(parent->get_g() + COST);
			existGrid->set_f(existGrid->get_g() + existGrid->get_f());
		}
		return existGrid;
	}
	else
	{
		//如果不在开放列表中，则加入开放列表
		EstGrid* grid = this->newGrid(next, dest, parent, COST);
		_openList.pushBack(grid);
		return grid;
	}
}

EstGrid* AStarPath::findTop(EstGrid& parent, Vec2& dest)
{
	Vec2 origin = parent.getPosInMap();
	Vec2 top(origin.x,origin.y - 1);
	//是否越界&&是否可通过
	if (top.y >= 0 && !_obstacle->getTileAt(top))
	{
		//当前点是否已经在封闭列表中
		if (!isContains(top, _closeList))
		{	//不在封闭列表中
			return TryNextGrid(&parent, dest, top);
		}
	}
	return nullptr;
}



EstGrid* AStarPath::findRight(EstGrid& parent, Vec2& dest)
{
	Vec2 origin = parent.getPosInMap();
	Vec2 right(origin.x + 1, origin.y);
	//是否越界&&是否可通过
	if (right.x < _line && !_obstacle->getTileAt(right))
	{
		//当前点是否已经在封闭列表中
		if (!isContains(right, _closeList))
		{
			return TryNextGrid(&parent, dest, right);
		}
	}
	return nullptr;
}

EstGrid* AStarPath::findBottom(EstGrid& parent, Vec2& dest)
{
	Vec2 origin = parent.getPosInMap();
	Vec2 bottom(origin.x, origin.y + 1);
	//是否越界&&是否可通过
	if (bottom.y < _row && !_obstacle->getTileAt(bottom))
	{
		//当前点是否已经在封闭列表中
		if (!isContains(bottom, _closeList))
		{
			return TryNextGrid(&parent, dest, bottom);
		}
	}
	return nullptr;
}

EstGrid* AStarPath::findLeft(EstGrid& parent, Vec2& dest)
{
	Vec2 origin = parent.getPosInMap();
	Vec2 left(origin.x-1, origin.y);
	//是否越界&&是否可通过
	if (left.x >= 0 && !_obstacle->getTileAt(left))
	{
		//当前点是否已经在封闭列表中
		if (!isContains(left, _closeList))
		{
			return TryNextGrid(&parent, dest, left);
		}	
	}
	return nullptr;
}

//EstGrid* AStarPath::findTopRight(EstGrid& parent, EstGrid& dest)
//{
//	return nullptr;
//}
//
//EstGrid* AStarPath::findBottomRight(EstGrid& parent, EstGrid& dest)
//{
//	
//	return nullptr;
//}
//
//
//EstGrid* AStarPath::findBottomLeft(EstGrid& parent, EstGrid& dest)
//{
//	return nullptr;
//}
//
//
//
//EstGrid* AStarPath::findTopLeft(EstGrid& parent, EstGrid& dest)
//{
//	return nullptr;
//}

int AStarPath::calcDestCost(Vec2& start, Vec2& end)
{
	int x = abs(start.x - end.x);
	int y = abs(start.y - end.y);
	
	return (x + y)*COST;
}

Vec2 AStarPath::posToMapBlock(Vec2& vec, TMXTiledMap* map)
{
	int x = vec.x / map->getTileSize().width;
	int y = (map->getContentSize().height - vec.y) / map->getTileSize().height;
	return Vec2(x, y);
}

void AStarPath::clear()
{
	if (!_openList.empty())
		_openList.clear();
	if (!_closeList.empty())
		_closeList.clear();
}

EstGrid* AStarPath::newGrid(Vec2& mapPos,Vec2& dest, EstGrid* parent,int spend)
{
	EstGrid* result = EstGrid::create(mapPos);
	result->set_gid(_road->getTileGIDAt(mapPos));
	if (parent)
	{
		//G值，办法就是找出该方格的父方格的G值，并根
		//据与父方格的相对位置（斜角或非斜角方向）来给这个G值加上 14 或者 10
		//暂时不考虑斜角,只有直走
		result->set_g(parent->get_g()+spend);
		//从给定方格到目的方格的估计移动开销
		result->set_h(calcDestCost(result->getPosInMap(), dest));
		result->set_f(result->get_g() + result->get_h());
		result->setParent(parent);
	}
	return result;
}

void AStarPath::openListToCloseList(EstGrid* grid)
{
	grid->retain();
	_openList.eraseObject(grid);
	_closeList.pushBack(grid);
	grid->release();
}
void AStarPath::closeListToOpenList(EstGrid* grid)
{
	grid->retain();
	_closeList.eraseObject(grid);
	_openList.pushBack(grid);
	grid->release();
}

EstGrid* AStarPath::findMinInOpenList()
{
	EstGrid* result = _openList.at(0);
	for (Vector<EstGrid*>::iterator begin = _openList.begin(); begin != _openList.end();begin++)
	{
		EstGrid* tmp = *begin;
		if ( tmp->operator<(*result))
		{
			result = tmp;
		}
	}
	return result;
}

bool AStarPath::isContains(Vec2& vec, Vector<EstGrid*>& list)
{
	for (EstGrid* tmp : list)
	{
		if (tmp->getPosInMap() == vec)
			return true;
	}
	return false;
}

EstGrid* AStarPath::findGridInList(Vec2& vec, Vector<EstGrid*>& list)
{
	for (EstGrid* tmp : list)
	{
		if (tmp->getPosInMap() == vec)
			return tmp;
	}
	return nullptr;
}

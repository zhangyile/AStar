#ifndef __HELLOWORLD_SCENE_H__
#define __HELLOWORLD_SCENE_H__

#include "cocos2d.h"
#include "EstGrid.h"
USING_NS_CC;
class HelloWorld : public cocos2d::Layer
{
public:
	HelloWorld();
	~HelloWorld();
    // there's no 'id' in cpp, so we recommend returning the class instance pointer
    static cocos2d::Scene* createScene();

    // Here's a difference. Method 'init' in cocos2d-x returns bool, instead of returning 'id' in cocos2d-iphone
    virtual bool init();
    
	void update(float delta)override;
    // implement the "static create()" method manually
    CREATE_FUNC(HelloWorld);

	bool onTouchBegan(Touch *touch, Event *unused_event)override;
	void onTouchMoved(Touch *touch, Event *unused_event)override;
	void onTouchEnded(Touch *touch, Event *unused_event)override;
	void onTouchCancelled(Touch *touch, Event *unused_event)override;

	LabelTTF* _label;
	TMXTiledMap* _map;
	CCSprite* _player;
	Vector<EstGrid*>* _path;
};

#endif // __HELLOWORLD_SCENE_H__

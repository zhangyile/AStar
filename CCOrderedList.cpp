#include "CCOrderedList.h"
USING_NS_CC;
template<class T>
void OrderList<T>::clear()
{
	for (Ref* ref : _data)
		ref->release();

	_data.clear();
}

template<class T>
void OrderList<T>::addRefForAllObjects()
{
	for (Ref* ref : _data)
		ref->retain();
}

template<class T>
OrderList<T>& OrderList<T>::operator = (OrderList<T>& other)
{
	if (this != &other) {
		CCLOGINFO("In the copy assignment operator!");
		clear();
		_data = other._data;
		addRefForAllObjects();
	}
	return *this;
}

template<class T>
OrderList<T>& OrderList<T>::operator = (const OrderList<T>& other)
{
	if (this != &other) {
		CCLOGINFO("In the copy assignment operator!");
		clear();
		_data = other._data;
		addRefForAllObjects();
	}
	return *this;
}

template<class T>
void OrderList<T>::insertElementByOrder(T data)
{
	if (_data.empty())
	{
		data->retain();
		_data.push_back(data);
	}
		
	else
	{
		std::list<int>::iterator front = _data.begin();
		std::list<int>::iterator after = ++_data.begin();

		if (*data <= **front)//data<=*front
		{
			_data.insert(front, data);
			return;
		}

		if (after == _data.end())
		{
			data->retain();
			*data < **front ? _data.insert(front, data) : _data.push_back(data);
			return;
		}

		do{
			if (*data > **front && *data <= **after)
			{
				_data.insert(after, data);
				return;
			}
			front++;
			after++;
		} while (after != _data.end());

		if (*data >= **front)
		{
			data->retain();
			_data.push_back(data);
		}
			
	}
}